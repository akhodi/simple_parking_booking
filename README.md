## _Simple Parking Booking_

Example on-site parking and online booking.

> Note: haven't implemented the authenticate feature (signup, signin, signout)
> we assume the user is logged in.

## Features

- User/Driver can have multiple cars
- Multiple spaces/places
- Each space/place can have many slots/bays
- Support on site parking & online booking
- Both parties (user & space) have transaction history

## Installation
```sh
cd sample_parking_booking
composer install
php artisan migrate
php artisan db:seed
php artisan serve
```
