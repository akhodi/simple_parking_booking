<?php

use Illuminate\Database\Seeder;

use App\Space;
use App\Slot;
use Faker\Factory as Faker;

class SpacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');

        $users = [];
        // create 2 spaces/places
        for ($i=1; $i<=2; $i++) {
            $space = Space::create([
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
                'name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'address' => $faker->address,
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude
            ]);

            // create 3 slot for each space/place
            for ($v = 1; $v <= 3; $v++) {
                Slot::create([
                    'created_at' => $faker->dateTime(),
                    'updated_at' => $faker->dateTime(),
                    'space_id' => $space->id,
                    'name' => $faker->name
                ]);
            }
        }
    }
}
