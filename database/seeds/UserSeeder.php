<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Vechile;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = (new \Faker\Factory())::create();
        $faker->addProvider(new \Faker\Provider\Fakecar($faker));

        $users = [];
        // create 2 user
        for ($i=1; $i<=2; $i++) {
            $user = User::create([
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
                'full_name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email
            ]);

            // create (random, 1-3) vechiles for each user
            for ($v = 1; $v <= rand(1,3); $v++) {
                Vechile::create([
                    'created_at' => $faker->dateTime(),
                    'updated_at' => $faker->dateTime(),
                    'user_id' => $user->id,
                    'name' => $faker->vehicle,
                    'registration_number' => $faker->vehicleRegistration
                ]);
            }
        }
    }
}
