<?php

use Illuminate\Database\Seeder;

use App\Pricing;
use Faker\Factory as Faker;

class PricingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create('en_US');
        $data = [
            [
                'min_hour' => 0,
                'max_hour' => 1,
                'price' => 0
            ],
            [
                'min_hour' => 1,
                'max_hour' => 2,
                'price' => 20
            ],
            [
                'min_hour' => 2,
                'max_hour' => 3,
                'price' => 60
            ],
            [
                'min_hour' => 3,
                'max_hour' => 4,
                'price' => 240
            ],
            [
                'min_hour' => 4,
                'max_hour' => 730001,
                'price' => 300
            ]
        ];

        foreach($data as $key) {
            Pricing::create([
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
                'min_hour' => $key['min_hour'],
                'max_hour' => $key['max_hour'],
                'price' => $key['price']
            ]);
        }
    }
}
