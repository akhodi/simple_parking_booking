<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('vechile_id')->unsigned();
            $table->foreign('vechile_id')->references('id')->on('vechiles');
            $table->integer('slot_id')->unsigned();
            $table->foreign('slot_id')->references('id')->on('slots');
            $table->enum('status', ['booked', 'check_in', 'check_out'])->default('booked');
            $table->dateTime('checkin_at')->nullable();
            $table->dateTime('checkout_at')->nullable();
            $table->decimal('price', 10, 2);
            $table->decimal('total_price', 10, 2);
            $table->boolean('is_paided')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
