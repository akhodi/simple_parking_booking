<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vechile extends Model
{
    protected $table = 'vechiles';
    protected $fillable = ['name', 'registration_number', 'user_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
