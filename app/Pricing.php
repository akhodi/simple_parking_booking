<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $table = 'pricings';
    protected $fillable = ['min_hour', 'max_hour', 'price'];
    protected $hidden = ['created_at', 'updated_at'];
}
