<?php

namespace App\Http\Controllers;

use App\Space;
use App\Slot;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;

class SpaceController extends BaseController
{
    // get available slot/bay by space
    public function slot_availables($id)
    {
        $slots = Slot::where([
            ['space_id', '=', $id],
            ['is_available', '=', true]
        ])
        ->orderBy('name', 'ASC')
        ->get();

        $slots = $slots->makeHidden('space_id');
        $message = $slots->count() > 0 ? 'Data retrieved successfully.' : 'Fully booked.';

        return $this->sendResponse($slots->toArray(), $message);
    }
}
