<?php

namespace App\Http\Controllers;

use App\Order;
use App\Pricing;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\BaseController as BaseController;

class CheckOutController extends BaseController
{
    function calculate_price($order_id) {
        $order = Order::where([
                ['id', '=', $order_id],
                ['status', '=', 'check_in'],
                ['checkout_at', '=', null]
            ])
            ->with([
                'vechile',
                'vechile.user'
            ])
            ->first();

        if (is_null($order)) {
            return $this->sendError('Ticket is invalid.');
        }

        $hours = $this->total_hour($order->checkin_at);

        $data = [
            'total_hours' => $hours,
            'total_price' => $this->total_price($hours),
            'order' => $order,
        ];

        return $this->sendResponse($data, 'Calculate price successfully.');
    }

    function pay($order_id) {
        $order = Order::where([
            ['id', '=', $order_id],
            ['status', '=', 'check_in'],
            ['checkout_at', '=', null]
        ])->first();

        if (is_null($order)) {
            return $this->sendError('Ticket is invalid.');
        }

        if ($order) {
            $hours = $this->total_hour($order->checkin_at);

            $order->checkout_at = Carbon::now();
            $order->status = 'check_out';
            $order->total_price = $this->total_price($hours);
            $order->is_paided = true;
            $order->save();

            // update slot/bay status to available
            app('App\Http\Controllers\BookingController')->slot_availability_update($order->slot_id, $order->status);
        }

        return $this->sendResponse($order, 'Check-out successfully.');
    }

    // calculate total hours
    function total_hour($checkin_at) {
        return Carbon::parse($checkin_at)->diffInHours(Carbon::now());
    }

    // calculate total price
    function total_price($hours) {
        $n = $hours > 0 ? ($hours - 1) : $hours;

        return Pricing::whereIn('min_hour', range(0, $n))
            ->sum('price');
    }
}
