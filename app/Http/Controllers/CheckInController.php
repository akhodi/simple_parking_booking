<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\BaseController as BaseController;

class CheckInController extends BaseController
{
    function check_in($order_id) {
        $order = Order::where([
            ['id', '=', $order_id],
            ['status', '=', 'booked'],
            ['checkin_at', '=', null]
        ])->first();

        if ($order) {
            $order->checkin_at = Carbon::now();
            $order->status = 'check_in';
            $order->expired_at = null;
            $order->save();
        }

        if (is_null($order)) {
            return $this->sendError('Ticket is invalid.');
        }

        return $this->sendResponse($order, 'Check-in successfully.');
    }
}
