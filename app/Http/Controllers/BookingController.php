<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderUser;
use App\OrderSpace;
use App\Vechile;
use App\Slot;
use Validator;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\BaseController as BaseController;

class BookingController extends BaseController
{
    // online booking before arriving at the location
    public function store(Request $request)
    {
        $input = $request->all();

        $vechile_id = $input['vechile_id'];
        $space_id = $input['space_id'];
        $slot_id = $input['slot_id'];

        $validator = Validator::make($input, [
            'vechile_id' => 'required|numeric',
            'space_id' => 'required|numeric'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors(), 200);
        }

        // validate vechile
        $vechile = Vechile::find($vechile_id);
        if (!$vechile) {
            return $this->sendError('Vechile not found.');
        }

        // check if a slot is available or occupied
        $slot_id = $input['slot_id'];
        if (!is_null($slot_id)) {

            $slot = Slot::find($slot_id);
            if (!$slot) {
                return $this->sendError('The slot/bay not found.');
            } else {
                if (!$slot->is_available) {
                    return $this->sendError('The slot/bay has occupied.');
                }

                if ($slot->space_id != $space_id) {
                    return $this->sendError('The slot/bay not found.');
                }
            }

        } else {

            $slot = $this->nearest_available_slot($space_id);
            if (is_null($slot)) {
                return $this->sendError('The slot/bay is fully booked.');
            }

            if (!$slot->is_available) {
                return $this->sendError('The slot/bay has occupied.');
            }
        }

        /* if after 15 minutes there is no checkin activity,
        the slot/bay will be automatically available again */
        $expired_at = Carbon::now()->addMinutes(15);
        /* it's just that the automatic function is not yet available,
        this is for future development */

        $order = Order::create([
            'vechile_id' => $vechile_id,
            'slot_id' => $slot->id,
            'expired_at' => $expired_at
        ]);

        if ($order) {

            $this->slot_availability_update($slot->id, $order->status);

            OrderUser::create([
                'order_id' => $order->id,
                'user_id' => $this->get_vechile($vechile_id)->user_id
            ]);

            OrderSpace::create([
                'order_id' => $order->id,
                'space_id' => $space_id
            ]);
        }

        return $this->sendResponse($order, 'Booking successfully.');
    }

    // get the nearest available slot/bay
    function nearest_available_slot($space_id) {
        return Slot::where([
            ['space_id', '=', $space_id],
            ['is_available', '=', true]
        ])
        ->orderBy('name', 'ASC')
        ->first();
    }

    // get vechile info
    function get_vechile($id) {
        return Vechile::find($id);
    }

    // update slot/bay status
    function slot_availability_update($slot_id, $order_status) {
        $slot = Slot::find($slot_id);

        $status = $order_status == 'check_out' ? 'available' : 'occupied';
        $is_available = $order_status == 'check_out' ? true : false;

        if ($slot) {
            $slot->is_available = $is_available;
            $slot->status = $status;
            $slot->save();
        }
    }
}
