<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = ['full_name', 'phone', 'email', 'is_active'];
    protected $hidden = ['created_at', 'updated_at'];
}
