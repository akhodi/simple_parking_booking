<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'vechile_id',
        'slot_id',
        'status',
        'checkin_at',
        'checkout_at',
        'total_price',
        'is_paided',
        'expired_at'
    ];
    protected $hidden = [];

    public function vechile()
    {
        return $this->belongsTo('App\Vechile');
    }

    public function slot()
    {
        return $this->belongsTo('App\Slot');
    }
}
