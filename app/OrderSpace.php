<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSpace extends Model
{
    protected $table = 'order_spaces';
    protected $fillable = [
        'order_id',
        'space_id'
    ];
    protected $hidden = [];
}
