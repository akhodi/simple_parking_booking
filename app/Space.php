<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    protected $table = 'spaces';
    protected $fillable = ['name', 'phone', 'email', 'address', 'latitude', 'longitude', 'is_active'];
    protected $hidden = ['created_at', 'updated_at'];
}
