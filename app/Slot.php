<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    protected $table = 'slots';
    protected $fillable = ['space_id', 'name', 'is_available', 'status'];
    protected $hidden = ['created_at', 'updated_at'];
}
