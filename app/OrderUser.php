<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderUser extends Model
{
    protected $table = 'order_users';
    protected $fillable = [
        'order_id',
        'user_id'
    ];
    protected $hidden = [];
}
