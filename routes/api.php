<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/spaces/{id}/slot-availables','SpaceController@slot_availables');
Route::post('/booking','BookingController@store');
Route::put('/check-in/{order_id}','CheckInController@check_in');
Route::get('/calculate-price/{order_id}','CheckOutController@calculate_price');
Route::put('/pay/{order_id}','CheckOutController@pay');
